﻿using System;

namespace app_of_variable
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
    
            Console.Clear();
            var name ="Vincent Ning";
            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine($"My name is {name}");
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
